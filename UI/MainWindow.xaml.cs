﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.Windows.Media.Animation;
using SlrrLib.View;

namespace CarUVGenerator
{
  public partial class MainWindow : Window
  {
    private const float smallStepUV = 0.001f;
    private const float largeStepUV = 0.1f;
    private const float offsetMultUV = 100.0f;
    private const float tickForMove = 0.01f;
    private string lastRpkFnam = "";
    private string lastOpenFileDialogDir = System.IO.Directory.GetCurrentDirectory();
    private string highlightTexture = "grid.png";
    private List<int> deletedIndices = new List<int>();
    private List<SlrrLib.Geom.NamedModel> hiddenParts = new List<SlrrLib.Geom.NamedModel>();

    public MainWindow()
    {
      InitializeComponent();
      if (System.IO.File.Exists("lastDir"))
        lastOpenFileDialogDir = System.IO.File.ReadAllText("lastDir");
      SlrrLib.Model.MessageLog.SetConsoleLogOutput();
    }

    private void refreshPivots()
    {
      ctrlListOfPivots.Items.Clear();
      foreach (var model in ctrlOrbitingViewport.CurrentModels)
        ctrlListOfPivots.Items.Add(model);
    }
    private void hideExtraParts()
    {
      foreach (var pivotModel in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedScxModel>().ToList())
      {
        if (pivotModel == null)
          continue;
        if (pivotModel.Scxv3Source == null &&
            pivotModel.Scxv4Source == null)
          continue;
        if (pivotModel.ScxFnam.Contains("_ExtraParts"))
        {
          hiddenParts.Add(pivotModel);
          ctrlOrbitingViewport.RemoveModelFromScene(pivotModel);
        }
      }
      foreach (var toRem in hiddenParts)
        ctrlListOfPivots.Items.Remove(toRem);
    }
    private void showExtraParts()
    {
      foreach (var toReAdd in hiddenParts)
      {
        if (!ctrlOrbitingViewport.CurrentModels.Contains(toReAdd))
        {
          ctrlOrbitingViewport.AddModelToScene(toReAdd);
          ctrlListOfPivots.Items.Add(toReAdd);
        }
      }
      hiddenParts.Clear();
    }
    private bool isModelValidPaintableMesh(SlrrLib.Geom.NamedScxModel pivotModel)
    {
      if (pivotModel == null)
        return false;
      if (pivotModel.Scxv3Source == null &&
          pivotModel.Scxv4Source == null)
        return false;
      if (!pivotModel.HasPaintableTexture)
        return false;
      return true;
    }
    private float maxLengthPositionInCurrentModels()
    {
      float maxLengthPos = float.MinValue;
      foreach (var pivotModel in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedScxModel>())
      {
        if (!isModelValidPaintableMesh(pivotModel))
          continue;
        float maxLengthPosLocal = float.MinValue;
        if (pivotModel.Scxv4Source != null)
        {
          var m = pivotModel.Scxv4Source.Meshes[pivotModel.MeshIndex];
          maxLengthPosLocal = (float)m.VertexData.VertexDataList.Max(
                                x => Math.Sqrt((x.Position.X * x.Position.X) +
                                               (x.Position.Y * x.Position.Y) +
                                               (x.Position.Z * x.Position.Z)));
        }
        else
        {
          var m = pivotModel.Scxv3Source.Meshes[pivotModel.MeshIndex];
          maxLengthPosLocal = (float)m.VertexDatas.Max(
                                x => Math.Sqrt((x.VertexCoordX * x.VertexCoordX) +
                                               (x.VertexCoordY * x.VertexCoordY) +
                                               (x.VertexCoordZ * x.VertexCoordZ)));
        }
        maxLengthPos = (float)Math.Max(maxLengthPosLocal, maxLengthPos);
      }
      return maxLengthPos;
    }
    private void generatePivotBoxUV()
    {
      var uvparams = parseUVGenerationParams();
      uvparams.offX -= 0.5f;
      uvparams.offY -= 0.5f;
      uvparams.offZ -= 0.5f;
      uvparams.rotationDefomrScaleYFront = uvparams.rotationDefomrScaleXFront;
      uvparams.rotationDefomrScaleZFront = uvparams.rotationDefomrScaleXFront;
      float maxLengthPosisition = maxLengthPositionInCurrentModels();
      foreach (var pivotModel in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedScxModel>())
      {
        if (!isModelValidPaintableMesh(pivotModel))
          continue;
        if (pivotModel.Scxv4Source != null)
        {
          var mesh = pivotModel.Scxv4Source.Meshes[pivotModel.MeshIndex];
          SlrrLib.Geom.UVUtil.GenerateUVBox(mesh, pivotModel.PaintableTextureUVIndex + 1, uvparams, maxLengthPosisition, pivotModel.ModelGeom.Transform);
        }
        if (pivotModel.Scxv3Source != null)
        {
          var mesh = pivotModel.Scxv3Source.Meshes[pivotModel.MeshIndex];
          SlrrLib.Geom.UVUtil.GenerateUVBox(mesh, pivotModel.PaintableTextureUVIndex + 1, uvparams, maxLengthPosisition, pivotModel.ModelGeom.Transform);
        }
      }
    }
    private void generatePivotSphereUV()
    {
      var uvparams = parseUVGenerationParams();
      List<Task> tasks = new List<Task>();
      foreach (var pivotModel in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedScxModel>())
      {
        if (!isModelValidPaintableMesh(pivotModel))
          continue;
        if (pivotModel.Scxv4Source != null)
        {
          var mesh = pivotModel.Scxv4Source.Meshes[pivotModel.MeshIndex];
          Transform3D transClone = null;
          if (pivotModel.ModelGeom.Transform != null)
          {
            transClone = pivotModel.ModelGeom.Transform.Clone();
            transClone.Freeze();
          }
          tasks.Add(Task.Run(() =>
          {
            SlrrLib.Geom.UVUtil.GenerateUVSphere(mesh, pivotModel.PaintableTextureUVIndex + 1, uvparams, transClone);
          }));
        }
        if (pivotModel.Scxv3Source != null)
        {
          var mesh = pivotModel.Scxv3Source.Meshes[pivotModel.MeshIndex];
          SlrrLib.Geom.UVUtil.GenerateUVSphere(mesh, pivotModel.PaintableTextureUVIndex + 1, uvparams, pivotModel.ModelGeom.Transform);
        }
      }
      Task.WaitAll(tasks.ToArray());
    }
    private void normalizeGeneratedUVs()
    {
      var uvBounds = calcUVBounds();
      foreach (var pivotModel in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedScxModel>())
      {
        if (!isModelValidPaintableMesh(pivotModel))
          continue;
        if (pivotModel.Scxv4Source != null)
        {
          var mesh = pivotModel.Scxv4Source.Meshes[pivotModel.MeshIndex];
          SlrrLib.Geom.UVUtil.GenerateUVNormalize(mesh, pivotModel.PaintableTextureUVIndex + 1, uvBounds);
        }
        if (pivotModel.Scxv3Source != null)
        {
          var mesh = pivotModel.Scxv3Source.Meshes[pivotModel.MeshIndex];
          SlrrLib.Geom.UVUtil.GenerateUVNormalize(mesh, pivotModel.PaintableTextureUVIndex + 1, uvBounds);
        }
      }
    }
    private float getProperUV<T>(int UVNum, int coord, T uvContainer)
    {
      if (uvContainer is SlrrLib.Model.DynamicCompleteVertexDataV4)
      {
        var v4 = uvContainer as SlrrLib.Model.DynamicCompleteVertexDataV4;
        if (coord == 1)
        {
          if (UVNum == 1)
            return v4.Uv1.U;
          if (UVNum == 2)
            return v4.Uv2.U;
          return v4.Uv3.U;
        }
        else
        {
          if (UVNum == 1)
            return v4.Uv1.V;
          if (UVNum == 2)
            return v4.Uv2.V;
          return v4.Uv3.V;
        }
      }
      else if (uvContainer is SlrrLib.Model.DynamicVertexV3)
      {
        var v3 = uvContainer as SlrrLib.Model.DynamicVertexV3;
        if (coord == 1)
        {
          if (UVNum == 1)
            return v3.UVChannel1X;
          if (UVNum == 2)
            return v3.UVChannel2X;
          return v3.UVChannel3X;
        }
        else
        {
          if (UVNum == 1)
            return v3.UVChannel1Y;
          if (UVNum == 2)
            return v3.UVChannel2Y;
          return v3.UVChannel3Y;
        }
      }
      return -1;
    }
    private SlrrLib.Geom.UVGenerationParams parseUVGenerationParams()
    {
      return new SlrrLib.Geom.UVGenerationParams
      {
        scale = UIUtil.ParseOrZero(ctrlTextBoxUVGenScale.Text),
        offX = UIUtil.ParseOrZero(ctrlTextBoxUVGenOffsetX.Text),
        offY = UIUtil.ParseOrZero(ctrlTextBoxUVGenOffsetY.Text),
        offZ = UIUtil.ParseOrZero(ctrlTextBoxUVGenOffsetZ.Text),
        scaleX = UIUtil.ParseOrZero(ctrlTextBoxUVGenScaleX.Text),
        scaleY = UIUtil.ParseOrZero(ctrlTextBoxUVGenScaleY.Text),
        scaleZ = UIUtil.ParseOrZero(ctrlTextBoxUVGenScaleZ.Text),
        rotationScaleFront = UIUtil.ParseOrZero(ctrlTextBoxUVRotationScaleFront.Text),
        rotationScaleMinPosFront = UIUtil.ParseOrZero(ctrlTextBoxUVRotationScaleMinPosFront.Text),
        rotationDefomrScaleXFront = UIUtil.ParseOrZero(ctrlTextBoxUVDeformXScaleFront.Text),
        rotationDefomrScaleYFront = UIUtil.ParseOrZero(ctrlTextBoxUVDeformYScaleFront.Text),
        rotationDefomrScaleZFront = UIUtil.ParseOrZero(ctrlTextBoxUVDeformZScaleFront.Text),
        rotationScaleBack = UIUtil.ParseOrZero(ctrlTextBoxUVRotationScaleBack.Text),
        rotationScaleMinPosBack = UIUtil.ParseOrZero(ctrlTextBoxUVRotationScaleMinPosBack.Text),
        rotationDefomrScaleXBack = UIUtil.ParseOrZero(ctrlTextBoxUVDeformXScaleBack.Text),
        rotationDefomrScaleYBack = UIUtil.ParseOrZero(ctrlTextBoxUVDeformYScaleBack.Text),
        rotationDefomrScaleZBack = UIUtil.ParseOrZero(ctrlTextBoxUVDeformZScaleBack.Text),
        rotateX = UIUtil.ParseOrZero(ctrlTextBoxUVRotateX.Text),
        rotateY = UIUtil.ParseOrZero(ctrlTextBoxUVRotateY.Text),
        rotateZ = UIUtil.ParseOrZero(ctrlTextBoxUVRotateZ.Text)
      };
    }
    private SlrrLib.Geom.UVCoordinateBounds calcUVBounds()
    {
      float globMaxU = float.MinValue;
      float globMaxV = float.MinValue;
      float globMinU = float.MaxValue;
      float globMinV = float.MaxValue;
      foreach (var pivotModel in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedScxModel>())
      {
        if (!isModelValidPaintableMesh(pivotModel))
          continue;
        float maxU = float.MinValue;
        float minU = float.MaxValue;
        float minV = float.MaxValue;
        float maxV = float.MinValue;
        if (pivotModel.Scxv4Source != null)
        {
          var m = pivotModel.Scxv4Source.Meshes[pivotModel.MeshIndex];
          var UVNum = pivotModel.PaintableTextureUVIndex + 1;
          maxU = (float)m.VertexData.VertexDataList.Max(x => getProperUV(UVNum, 1, x));
          minU = (float)m.VertexData.VertexDataList.Min(x => getProperUV(UVNum, 1, x));
          maxV = (float)m.VertexData.VertexDataList.Max(x => getProperUV(UVNum, 2, x));
          minV = (float)m.VertexData.VertexDataList.Min(x => getProperUV(UVNum, 2, x));
        }
        else
        {
          var m = pivotModel.Scxv3Source.Meshes[pivotModel.MeshIndex];
          var UVNum = pivotModel.PaintableTextureUVIndex + 1;
          maxU = (float)m.VertexDatas.Max(x => getProperUV(UVNum, 1, x));
          minU = (float)m.VertexDatas.Min(x => getProperUV(UVNum, 1, x));
          maxV = (float)m.VertexDatas.Max(x => getProperUV(UVNum, 2, x));
          minV = (float)m.VertexDatas.Min(x => getProperUV(UVNum, 2, x));
        }
        globMaxU = (float)Math.Max(globMaxU, maxU);
        globMaxV = (float)Math.Max(globMaxV, maxV);
        globMinU = (float)Math.Min(globMinU, minU);
        globMinV = (float)Math.Min(globMinV, minV);
      }
      return new SlrrLib.Geom.UVCoordinateBounds
      {
        maxU = globMaxU,
        maxV = globMaxV,
        minU = globMinU,
        minV = globMinV
      };
    }
    private void textBoxMouseWheelGenUV(TextBox ctrTextBox, float stepMult, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrTextBox.Text, out float cur))
      {
        cur += Math.Sign(e.Delta) * tickForMove * (Keyboard.IsKeyDown(Key.LeftShift) ? largeStepUV * stepMult * offsetMultUV : smallStepUV * offsetMultUV * stepMult);
        ctrTextBox.Text = UIUtil.FloatToString(cur);
        generateUVs();
      }
    }
    private void generateUVs()
    {
      var selectedAlg = (ctrlComboUVGenAlg.SelectedItem as ComboBoxItem).Content as string;
      if (selectedAlg == "Pivot-Box")
      {
        generatePivotBoxUV();
      }
      else if (selectedAlg == "Pivot-Sphere")
      {
        generatePivotSphereUV();
      }
      if (selectedAlg == "NormalizeUV" || ctrlCheckBoxUVAutoNorm.IsChecked.GetValueOrDefault())
      {
        normalizeGeneratedUVs();
      }
      ctrlOrbitingViewport.ExtractUVsForPaintableModels();
    }
    private string pathToProjectionName(string path)
    {
      string result = System.IO.Path.GetFullPath(path);
      int carsIndex = result.IndexOf("cars");
      result = result.Substring(carsIndex);
      return result.Replace("\\", "_") + ".prjcfg";
    }
    private void manageHighlighting()
    {
      if (ctrlListOfPivots.SelectedItems.Count == 0)
      {
        ctrlOrbitingViewport.SetTextureForPaintableModels(highlightTexture, 1, 0.7, Colors.Red);
        return;
      }
      ctrlOrbitingViewport.SetAllModelsToTexture(@"grid_gray.png", 0.5, 0.5, Colors.DarkCyan);
      foreach (var selItem in ctrlListOfPivots.SelectedItems)
      {
        var newlySelectedItem = selItem as SlrrLib.Geom.NamedModel;
        ctrlOrbitingViewport.SetModelToTexture(newlySelectedItem, highlightTexture, 1, 0.7, Colors.Red);
      }
    }

    private void ctrlButtonRenderScene_Click(object sender, RoutedEventArgs e)
    {
      Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
      {
        InitialDirectory = lastOpenFileDialogDir,
        FileName = "",
        DefaultExt = ".scx",
        Filter = "RPKs|*.rpk"
      };
      var result = dlg.ShowDialog();
      lastRpkFnam = "";
      if (result == true)
      {
        lastRpkFnam = dlg.FileName;
        lastOpenFileDialogDir = System.IO.Path.GetDirectoryName(lastRpkFnam);
      }
      else
      {
        return;
      }
      Title = lastRpkFnam;
      ctrlListOfPivots.SelectionMode = SelectionMode.Single;
      System.IO.File.WriteAllText("lastDir", lastOpenFileDialogDir);
      ctrlOrbitingViewport.RenderScene(new SlrrLib.Geom.CarRpkModelFactory(lastRpkFnam));
      refreshPivots();
      ctrlListOfPivots.SelectedIndex = -1;
      ctrlOrbitingViewport.ExtractUVsForPaintableModels();
      deletedIndices = new List<int>();
      manageHighlighting();
    }
    private void ctrlListOfPivots_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      manageHighlighting();
    }
    private void ctrlButtonGenUV_Click(object sender, RoutedEventArgs e)
    {
      generateUVs();
    }
    private void ctrlButtonUVGenSaveModel_Click(object sender, RoutedEventArgs e)
    {
      ctrlButtonSaveProjection_Click(null, null);
      foreach (var pivotModel in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedScxModel>())
      {
        if (!isModelValidPaintableMesh(pivotModel))
          continue;
        if (pivotModel.Scxv4Source != null)
        {
          pivotModel.Scxv4Source.SaveAs(pivotModel.ScxFnam, true, "_BAK_UVWorks_");
        }
        else
        {
          pivotModel.Scxv3Source.SaveAs(pivotModel.ScxFnam, true, "_BAK_UVWorks_");
        }
      }
    }
    private void ctrlTextBoxUVGenScale_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 10.0f, e);
    }
    private void ctrlTextBoxUVGenOffsetX_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 10.0f, e);
    }
    private void ctrlTextBoxUVGenOffsetY_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 10.0f, e);
    }
    private void ctrlTextBoxUVGenOffsetZ_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 10.0f, e);
    }
    private void ctrlTextBoxUVGenScaleX_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 1.0f, e);
    }
    private void ctrlTextBoxUVGenScaleY_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 1.0f, e);
    }
    private void ctrlTextBoxUVGenScaleZ_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 1.0f, e);
    }
    private void ctrlButtonAutoParams_Click(object sender, RoutedEventArgs e)
    {
      if (ctrlOrbitingViewport.SelectedModel == null)
        return;
      if (!(ctrlOrbitingViewport.SelectedModel is SlrrLib.Geom.NamedScxModel))
        return;
      var scxModel = ctrlOrbitingViewport.SelectedModel as SlrrLib.Geom.NamedScxModel;
      if (scxModel.Scxv4Source != null)
      {
        var selectedAlg = (ctrlComboUVGenAlg.SelectedItem as ComboBoxItem).Content as string;
        var m = scxModel.Scxv4Source.Meshes[scxModel.MeshIndex];
        float maxX = (float)m.VertexData.VertexDataList.Max(x =>
        {
          return x.Position.X;
        });
        float maxY = (float)m.VertexData.VertexDataList.Max(x =>
        {
          return x.Position.Y;
        });
        float maxZ = (float)m.VertexData.VertexDataList.Max(x =>
        {
          return x.Position.Z;
        });

        float minX = (float)m.VertexData.VertexDataList.Min(x =>
        {
          return x.Position.X;
        });
        float minY = (float)m.VertexData.VertexDataList.Min(x =>
        {
          return x.Position.Y;
        });
        float minZ = (float)m.VertexData.VertexDataList.Min(x =>
        {
          return x.Position.Z;
        });

        float offX = 0.0f;
        float offY = 0.0f;
        float offZ = 0.0f;
        float scaleX = 1.0f / (maxX - minX);
        float scaleY = 1.0f / (maxY - minY);
        float scaleZ = 1.0f / (maxZ - minZ);
        scaleX = scaleY = scaleZ = (float)Math.Min(Math.Min(scaleX, scaleY), scaleZ);

        ctrlTextBoxUVGenOffsetX.Text = UIUtil.FloatToString(offX);
        ctrlTextBoxUVGenOffsetY.Text = UIUtil.FloatToString(offY);
        ctrlTextBoxUVGenOffsetZ.Text = UIUtil.FloatToString(offZ);
        ctrlTextBoxUVGenScaleX.Text = UIUtil.FloatToString(scaleX);
        ctrlTextBoxUVGenScaleY.Text = UIUtil.FloatToString(scaleY);
        ctrlTextBoxUVGenScaleZ.Text = UIUtil.FloatToString(scaleZ);

        ctrlComboUVGenAlg.SelectedIndex = 1;
        generateUVs();
        ctrlComboUVGenAlg.SelectedIndex = 2;
        generateUVs();
      }

      // FEATURE: SCXV3 Missing
    }
    private void ctrlTextBoxUVDeformXScaleFront_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 1.0f, e);
    }
    private void ctrlTextBoxUVDeformYScaleFront_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 1.0f, e);
    }
    private void ctrlTextBoxUVDeformZScaleFront_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 1.0f, e);
    }
    private void ctrlTextBoxUVRotationScaleFront_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 0.1f, e);
    }
    private void ctrlTextBoxUVRotationScaleMinPosFront_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 100.0f, e);
    }
    private void ctrlTextBoxUVDeformXScaleBack_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 1.0f, e);
    }
    private void ctrlTextBoxUVDeformYScaleBack_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 1.0f, e);
    }
    private void ctrlTextBoxUVDeformZScaleBack_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 1.0f, e);
    }
    private void ctrlTextBoxUVRotationScaleBack_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 0.1f, e);
    }
    private void ctrlTextBoxUVRotationScaleMinPosBack_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 100.0f, e);
    }
    private void ctrlTextBoxUVRotateX_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 10.0f, e);
    }
    private void ctrlTextBoxUVRotateY_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 10.0f, e);
    }
    private void ctrlTextBoxUVRotateZ_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      textBoxMouseWheelGenUV(sender as TextBox, 10.0f, e);
    }
    private void ctrlButtonFlipNormals_Click(object sender, RoutedEventArgs e)
    {
      foreach (var pivotModel in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedScxModel>())
      {
        if (pivotModel == null)
          continue;
        if (pivotModel.Scxv3Source == null &&
            pivotModel.Scxv4Source == null)
          continue;
        var model3D = pivotModel.ModelGeom as GeometryModel3D;
        var meshGeom = model3D.Geometry as MeshGeometry3D;
        meshGeom.TriangleIndices = new Int32Collection(meshGeom.TriangleIndices.Reverse());
        meshGeom.Normals = new Vector3DCollection(meshGeom.Normals.Select(x => -x));
      }
    }
    private void ctrlListOfPivots_KeyUp(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Delete)
      {
        foreach (var selPivot in ctrlListOfPivots.SelectedItems.Cast<SlrrLib.Geom.NamedModel>().ToList())
        {
          if (ctrlListOfPivots.SelectedIndex >= 0 && ctrlListOfPivots.SelectedIndex < ctrlListOfPivots.Items.Count)
          {
            ctrlOrbitingViewport.RemoveModelFromScene(selPivot);
            ctrlListOfPivots.Items.Remove(selPivot);
          }
        }
      }
    }
    private void ctrlButtonChooseTexture_Click(object sender, RoutedEventArgs e)
    {
      Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
      {
        InitialDirectory = System.IO.Directory.GetCurrentDirectory(),
        FileName = "",
        DefaultExt = ".png",
        Filter = "PNGs|*.png|BMPs|*.bmp|JPGs|*.jpg"
      };

      var result = dlg.ShowDialog();

      if (result == true)
      {
        highlightTexture = dlg.FileName;
      }
      else
      {
        return;
      }
      manageHighlighting();
    }
    private void ctrlUVSaveAllAsObj_Click(object sender, RoutedEventArgs e)
    {
      SlrrLib.Geom.VRML97GeomExport exporter = new SlrrLib.Geom.VRML97GeomExport();
      var dlg = new Microsoft.Win32.SaveFileDialog
      {
        InitialDirectory = lastOpenFileDialogDir,
        FileName = "",
        DefaultExt = ".wrl",
        Filter = "wrls|*.wrl"
      };
      var result = dlg.ShowDialog();
      if (result == true)
      {
        foreach (var pivotObj in ctrlOrbitingViewport.CurrentModels)
        {
          if (pivotObj == null)
            continue;
          SlrrLib.Model.MessageLog.AddMessage(pivotObj.Name);
          exporter.PushModel(pivotObj);
        }
        exporter.WriteFullExport(dlg.FileName);
      }
    }
    private void ctrlButtonSaveProjection_Click(object sender, RoutedEventArgs e)
    {
      StringBuilder sb = new StringBuilder();
      sb.AppendLine(ctrlTextBoxUVGenScale.Text);
      sb.AppendLine(ctrlTextBoxUVGenOffsetX.Text);
      sb.AppendLine(ctrlTextBoxUVGenOffsetY.Text);
      sb.AppendLine(ctrlTextBoxUVGenOffsetZ.Text);
      sb.AppendLine(ctrlTextBoxUVGenScaleX.Text);
      sb.AppendLine(ctrlTextBoxUVGenScaleY.Text);
      sb.AppendLine(ctrlTextBoxUVGenScaleZ.Text);
      sb.AppendLine(ctrlTextBoxUVRotationScaleFront.Text);
      sb.AppendLine(ctrlTextBoxUVRotationScaleMinPosFront.Text);
      sb.AppendLine(ctrlTextBoxUVDeformXScaleFront.Text);
      sb.AppendLine(ctrlTextBoxUVDeformYScaleFront.Text);
      sb.AppendLine(ctrlTextBoxUVDeformZScaleFront.Text);
      sb.AppendLine(ctrlTextBoxUVRotationScaleBack.Text);
      sb.AppendLine(ctrlTextBoxUVRotationScaleMinPosBack.Text);
      sb.AppendLine(ctrlTextBoxUVDeformXScaleBack.Text);
      sb.AppendLine(ctrlTextBoxUVDeformYScaleBack.Text);
      sb.AppendLine(ctrlTextBoxUVDeformZScaleBack.Text);
      sb.AppendLine(ctrlTextBoxUVRotateX.Text);
      sb.AppendLine(ctrlTextBoxUVRotateY.Text);
      sb.AppendLine(ctrlTextBoxUVRotateZ.Text);
      string defaultName = pathToProjectionName(lastRpkFnam);
      var dlg = new Microsoft.Win32.SaveFileDialog
      {
        InitialDirectory = System.IO.Directory.GetCurrentDirectory(),
        FileName = defaultName,
        DefaultExt = ".prjcfg",
        Filter = "prjcfg|*.prjcfg"
      };
      bool ctrPressed = Keyboard.IsKeyDown(Key.LeftCtrl);
      if (ctrPressed)
      {
        var result = dlg.ShowDialog();
        if (result == true)
        {
          System.IO.File.WriteAllText(dlg.FileName, sb.ToString());
        }
      }
      else
      {
        System.IO.File.WriteAllText(defaultName, sb.ToString());
      }
    }
    private void ctrlButtonLoadProjection_Click(object sender, RoutedEventArgs e)
    {
      string defaultName = pathToProjectionName(lastRpkFnam);
      Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
      {
        InitialDirectory = System.IO.Directory.GetCurrentDirectory(),
        FileName = defaultName,
        DefaultExt = ".prjcfg",
        Filter = "prjcfg|*.prjcfg"
      };
      bool ctrPressed = Keyboard.IsKeyDown(Key.LeftCtrl);
      bool shoudLoad = false;
      if (ctrPressed)
      {
        var result = dlg.ShowDialog();
        if (result == true)
        {
          shoudLoad = true;
        }
      }
      else
      {
        shoudLoad = true;
      }
      if (shoudLoad && System.IO.File.Exists(dlg.FileName))
      {
        var lns = System.IO.File.ReadAllLines(dlg.FileName);
        if (lns.Length < 20)
        {
          SlrrLib.Model.MessageLog.AddError("Projection config file contains " + lns.Length.ToString() + " lines which should be at least 20");
          return;
        }
        ctrlTextBoxUVGenScale.Text = lns[0];
        ctrlTextBoxUVGenOffsetX.Text = lns[1];
        ctrlTextBoxUVGenOffsetY.Text = lns[2];
        ctrlTextBoxUVGenOffsetZ.Text = lns[3];
        ctrlTextBoxUVGenScaleX.Text = lns[4];
        ctrlTextBoxUVGenScaleY.Text = lns[5];
        ctrlTextBoxUVGenScaleZ.Text = lns[6];
        ctrlTextBoxUVRotationScaleFront.Text = lns[7];
        ctrlTextBoxUVRotationScaleMinPosFront.Text = lns[8];
        ctrlTextBoxUVDeformXScaleFront.Text = lns[9];
        ctrlTextBoxUVDeformYScaleFront.Text = lns[10];
        ctrlTextBoxUVDeformZScaleFront.Text = lns[11];
        ctrlTextBoxUVRotationScaleBack.Text = lns[12];
        ctrlTextBoxUVRotationScaleMinPosBack.Text = lns[13];
        ctrlTextBoxUVDeformXScaleBack.Text = lns[14];
        ctrlTextBoxUVDeformYScaleBack.Text = lns[15];
        ctrlTextBoxUVDeformZScaleBack.Text = lns[16];
        ctrlTextBoxUVRotateX.Text = lns[17];
        ctrlTextBoxUVRotateY.Text = lns[18];
        ctrlTextBoxUVRotateZ.Text = lns[19];
      }
    }
    private void ctrlButtonHideExtraParts_Click(object sender, RoutedEventArgs e)
    {
      hideExtraParts();
    }
    private void ctrlButtonShowExtraParts_Click(object sender, RoutedEventArgs e)
    {
      showExtraParts();
    }
  }
}
