Car UV Generator
=======

This app is part of the suite designed for the Street Legal Racing: Redline game (https://en.wikipedia.org/wiki/Street_Legal_Racing:_Redline).
The programs and their development is discussed on the vStanced forums (http://vstanced.com/viewtopic.php?f=68&t=13249).

The program regenerates the UV mapping of all the models in a vehicle definition taking their positioning into account which leads to an UV mapping which allows the usage of a single texture without visible seams on all the parts. This is done by supplying parameters to the UV generation on a UI which includes a 3D representation of the whole vehicle model with all its parts attached. The app is based on the SlrrLib (https://gitlab.com/slrr/slrr-lib) project.

## Building

You'll need Visual Studio 2019 or higher to build Car UV Generator.

1. Clone this repository
2. Open the Visual Studio solution
3. Select either the target platform and build the solution (needed files will be copied over to the target directory).

## Contributing

Any contributions to the project are welcomed, it's recommended to use GitLab [merge requests](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).

## License

All source code in this repository is licensed under a [BSD 3-clause license](LICENSE.md).